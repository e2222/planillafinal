defmodule Planillafinal.PlanillaTest do
  use Planillafinal.DataCase

  alias Planillafinal.Planilla

  describe "empleados" do
    alias Planillafinal.Planilla.Empleado

    import Planillafinal.PlanillaFixtures

    @invalid_attrs %{edad: nil, nombre: nil, profesion: nil, sexo: nil, sueldo: nil}

    test "list_empleados/0 returns all empleados" do
      empleado = empleado_fixture()
      assert Planilla.list_empleados() == [empleado]
    end

    test "get_empleado!/1 returns the empleado with given id" do
      empleado = empleado_fixture()
      assert Planilla.get_empleado!(empleado.id) == empleado
    end

    test "create_empleado/1 with valid data creates a empleado" do
      valid_attrs = %{edad: 42, nombre: "some nombre", profesion: "some profesion", sexo: "some sexo", sueldo: "120.5"}

      assert {:ok, %Empleado{} = empleado} = Planilla.create_empleado(valid_attrs)
      assert empleado.edad == 42
      assert empleado.nombre == "some nombre"
      assert empleado.profesion == "some profesion"
      assert empleado.sexo == "some sexo"
      assert empleado.sueldo == Decimal.new("120.5")
    end

    test "create_empleado/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Planilla.create_empleado(@invalid_attrs)
    end

    test "update_empleado/2 with valid data updates the empleado" do
      empleado = empleado_fixture()
      update_attrs = %{edad: 43, nombre: "some updated nombre", profesion: "some updated profesion", sexo: "some updated sexo", sueldo: "456.7"}

      assert {:ok, %Empleado{} = empleado} = Planilla.update_empleado(empleado, update_attrs)
      assert empleado.edad == 43
      assert empleado.nombre == "some updated nombre"
      assert empleado.profesion == "some updated profesion"
      assert empleado.sexo == "some updated sexo"
      assert empleado.sueldo == Decimal.new("456.7")
    end

    test "update_empleado/2 with invalid data returns error changeset" do
      empleado = empleado_fixture()
      assert {:error, %Ecto.Changeset{}} = Planilla.update_empleado(empleado, @invalid_attrs)
      assert empleado == Planilla.get_empleado!(empleado.id)
    end

    test "delete_empleado/1 deletes the empleado" do
      empleado = empleado_fixture()
      assert {:ok, %Empleado{}} = Planilla.delete_empleado(empleado)
      assert_raise Ecto.NoResultsError, fn -> Planilla.get_empleado!(empleado.id) end
    end

    test "change_empleado/1 returns a empleado changeset" do
      empleado = empleado_fixture()
      assert %Ecto.Changeset{} = Planilla.change_empleado(empleado)
    end
  end

  describe "empresas" do
    alias Planillafinal.Planilla.Empresa

    import Planillafinal.PlanillaFixtures

    @invalid_attrs %{nombre: nil}

    test "list_empresas/0 returns all empresas" do
      empresa = empresa_fixture()
      assert Planilla.list_empresas() == [empresa]
    end

    test "get_empresa!/1 returns the empresa with given id" do
      empresa = empresa_fixture()
      assert Planilla.get_empresa!(empresa.id) == empresa
    end

    test "create_empresa/1 with valid data creates a empresa" do
      valid_attrs = %{nombre: "some nombre"}

      assert {:ok, %Empresa{} = empresa} = Planilla.create_empresa(valid_attrs)
      assert empresa.nombre == "some nombre"
    end

    test "create_empresa/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Planilla.create_empresa(@invalid_attrs)
    end

    test "update_empresa/2 with valid data updates the empresa" do
      empresa = empresa_fixture()
      update_attrs = %{nombre: "some updated nombre"}

      assert {:ok, %Empresa{} = empresa} = Planilla.update_empresa(empresa, update_attrs)
      assert empresa.nombre == "some updated nombre"
    end

    test "update_empresa/2 with invalid data returns error changeset" do
      empresa = empresa_fixture()
      assert {:error, %Ecto.Changeset{}} = Planilla.update_empresa(empresa, @invalid_attrs)
      assert empresa == Planilla.get_empresa!(empresa.id)
    end

    test "delete_empresa/1 deletes the empresa" do
      empresa = empresa_fixture()
      assert {:ok, %Empresa{}} = Planilla.delete_empresa(empresa)
      assert_raise Ecto.NoResultsError, fn -> Planilla.get_empresa!(empresa.id) end
    end

    test "change_empresa/1 returns a empresa changeset" do
      empresa = empresa_fixture()
      assert %Ecto.Changeset{} = Planilla.change_empresa(empresa)
    end
  end
end
