defmodule PlanillafinalWeb.EmpleadoControllerTest do
  use PlanillafinalWeb.ConnCase

  import Planillafinal.PlanillaFixtures

  @create_attrs %{edad: 42, nombre: "some nombre", profesion: "some profesion", sexo: "some sexo", sueldo: "120.5"}
  @update_attrs %{edad: 43, nombre: "some updated nombre", profesion: "some updated profesion", sexo: "some updated sexo", sueldo: "456.7"}
  @invalid_attrs %{edad: nil, nombre: nil, profesion: nil, sexo: nil, sueldo: nil}

  describe "index" do
    test "lists all empleados", %{conn: conn} do
      conn = get(conn, Routes.empleado_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Empleados"
    end
  end

  describe "new empleado" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.empleado_path(conn, :new))
      assert html_response(conn, 200) =~ "New Empleado"
    end
  end

  describe "create empleado" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.empleado_path(conn, :create), empleado: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.empleado_path(conn, :show, id)

      conn = get(conn, Routes.empleado_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Empleado"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.empleado_path(conn, :create), empleado: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Empleado"
    end
  end

  describe "edit empleado" do
    setup [:create_empleado]

    test "renders form for editing chosen empleado", %{conn: conn, empleado: empleado} do
      conn = get(conn, Routes.empleado_path(conn, :edit, empleado))
      assert html_response(conn, 200) =~ "Edit Empleado"
    end
  end

  describe "update empleado" do
    setup [:create_empleado]

    test "redirects when data is valid", %{conn: conn, empleado: empleado} do
      conn = put(conn, Routes.empleado_path(conn, :update, empleado), empleado: @update_attrs)
      assert redirected_to(conn) == Routes.empleado_path(conn, :show, empleado)

      conn = get(conn, Routes.empleado_path(conn, :show, empleado))
      assert html_response(conn, 200) =~ "some updated nombre"
    end

    test "renders errors when data is invalid", %{conn: conn, empleado: empleado} do
      conn = put(conn, Routes.empleado_path(conn, :update, empleado), empleado: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Empleado"
    end
  end

  describe "delete empleado" do
    setup [:create_empleado]

    test "deletes chosen empleado", %{conn: conn, empleado: empleado} do
      conn = delete(conn, Routes.empleado_path(conn, :delete, empleado))
      assert redirected_to(conn) == Routes.empleado_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.empleado_path(conn, :show, empleado))
      end
    end
  end

  defp create_empleado(_) do
    empleado = empleado_fixture()
    %{empleado: empleado}
  end
end
