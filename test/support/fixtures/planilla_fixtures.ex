defmodule Planillafinal.PlanillaFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Planillafinal.Planilla` context.
  """

  @doc """
  Generate a empleado.
  """
  def empleado_fixture(attrs \\ %{}) do
    {:ok, empleado} =
      attrs
      |> Enum.into(%{
        edad: 42,
        nombre: "some nombre",
        profesion: "some profesion",
        sexo: "some sexo",
        sueldo: "120.5"
      })
      |> Planillafinal.Planilla.create_empleado()

    empleado
  end

  @doc """
  Generate a empresa.
  """
  def empresa_fixture(attrs \\ %{}) do
    {:ok, empresa} =
      attrs
      |> Enum.into(%{
        nombre: "some nombre"
      })
      |> Planillafinal.Planilla.create_empresa()

    empresa
  end
end
