defmodule Planillafinal.Repo.Migrations.CreateEmpleados do
  use Ecto.Migration

  def change do
    create table(:empleados) do
      add :nombre, :string
      add :edad, :integer
      add :sexo, :string
      add :profesion, :string
      add :sueldo, :decimal
      add :empresa, :integer

      timestamps()
    end
  end
end
