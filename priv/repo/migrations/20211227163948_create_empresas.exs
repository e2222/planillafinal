defmodule Planillafinal.Repo.Migrations.CreateEmpresas do
  use Ecto.Migration

  def change do
    create table(:empresas) do
      add :nombre, :string

      timestamps()
    end
  end
end
