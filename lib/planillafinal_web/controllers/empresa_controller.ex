defmodule PlanillafinalWeb.EmpresaController do
  use PlanillafinalWeb, :controller

  alias Planillafinal.Planilla
  alias Planillafinal.Planilla.Empresa


  def index(conn, _params) do
    empresas = Planilla.list_empresas()
    render(conn, "index.html", empresas: empresas)
  end

  def new(conn, _params) do
    changeset = Planilla.change_empresa(%Empresa{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"empresa" => empresa_params}) do
    case Planilla.create_empresa(empresa_params) do
      {:ok, empresa} ->
        conn
        |> put_flash(:info, "Empresa created successfully.")
        |> redirect(to: Routes.empresa_path(conn, :show, empresa))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    empresa = Planilla.get_empresa!(id)
    render(conn, "show.html", empresa: empresa)
  end

  def edit(conn, %{"id" => id}) do
    empresa = Planilla.get_empresa!(id)
    changeset = Planilla.change_empresa(empresa)
    render(conn, "edit.html", empresa: empresa, changeset: changeset)
  end

  def update(conn, %{"id" => id, "empresa" => empresa_params}) do
    empresa = Planilla.get_empresa!(id)

    case Planilla.update_empresa(empresa, empresa_params) do
      {:ok, empresa} ->
        conn
        |> put_flash(:info, "Empresa updated successfully.")
        |> redirect(to: Routes.empresa_path(conn, :show, empresa))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", empresa: empresa, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    empresa = Planilla.get_empresa!(id)
    {:ok, _empresa} = Planilla.delete_empresa(empresa)

    conn
    |> put_flash(:info, "Empresa deleted successfully.")
    |> redirect(to: Routes.empresa_path(conn, :index))
  end



  def dashboard(conn, %{"id" => id}) do
    empresa = Planilla.get_empresa!(id)
    render(conn, "dashboard.html", empresa: empresa)
  end


  def consulta(conn, %{"id" => id, "opcion" => opcion, "moneda" => moneda }) do
    empresa = Planilla.get_empresa!(id)

    case opcion do

      "1" -> lista_empleados(conn, empresa)
      "2" -> lista_nombres(conn, empresa)
      "3" -> lista_edades(conn, empresa)
      "4" -> lista_profesiones(conn, empresa)
      "5" -> lista_sexos(conn, empresa)
      "6" -> lista_sueldos(conn, empresa)
      "7" -> calcular_presupuesto(conn, empresa, moneda)

    end


  end



  defp lista_empleados(conn, empresa) do
    resultado = Planilla.list_empleados_by_empresa(empresa.id)
    render(conn, "consultas/lista_empleados.html", empresa: empresa, empleados: resultado)
  end


  defp lista_nombres(conn, empresa) do
    lista = Planilla.list_empleados_by_empresa(empresa.id)
    resultado = for %Planillafinal.Planilla.Empleado{nombre: nombre} <- lista, do: nombre
    render(conn, "consultas/lista_nombres.html", empresa: empresa, nombres: resultado)
  end


  defp lista_edades(conn, empresa) do
    lista = Planilla.list_empleados_by_empresa(empresa.id)
    resultado = for %Planillafinal.Planilla.Empleado{edad: edad} <- lista, do: "#{edad}"
    render(conn, "consultas/lista_edades.html", empresa: empresa, edades: resultado)
  end

  def lista_profesiones(conn, empresa) do
    lista = Planilla.list_empleados_by_empresa(empresa.id)
    listaprofesiones = for %Planilla.Empleado{profesion: profesion} <- lista, do: profesion
    resultado=Enum.frequencies(listaprofesiones)
    |> Map.to_list()
    render(conn, "consultas/lista_profesiones.html", empresa: empresa, profesiones: resultado)
  end


  defp lista_sexos(conn, empresa) do
    lista = Planilla.list_empleados_by_empresa(empresa.id)
    listasexos = for %Planilla.Empleado{sexo: sexo} <- lista, do: sexo
    resultado=Enum.frequencies(listasexos)
    |> Map.to_list()
    render(conn, "consultas/lista_sexos.html", empresa: empresa, sexos: resultado)
  end


  defp lista_sueldos(conn, empresa) do
    lista = Planilla.list_empleados_by_empresa(empresa.id)
    resultado = for %Planilla.Empleado{sueldo: sueldo} <- lista, do: sueldo
    render(conn, "consultas/lista_sueldos.html", empresa: empresa, sueldos: resultado)
  end


  defp calcular_presupuesto(conn, empresa, moneda) do

    lista = Planilla.list_empleados_by_empresa(empresa.id)
    listasueldos = for %Planilla.Empleado{sueldo: sueldo} <- lista, do: Decimal.to_float(sueldo)

    total = listasueldos
    |> Enum.sum()
    |> Float.ceil(2)

    resultado= case moneda do

      "USD" -> {"$", total
            |> :erlang.float_to_binary([decimals: 2])
            |> String.replace("\"", "")}

      "EUR" -> {"€", total * 0.88
            |> :erlang.float_to_binary([decimals: 2])
            |> String.replace("\"", "")}

      "GBP" -> {"£", total * 0.74
            |> :erlang.float_to_binary([decimals: 2])
            |> String.replace("\"", "")}

    end

    render(conn, "consultas/presupuesto.html", empresa: empresa, presupuesto: resultado)

  end










end
