defmodule PlanillafinalWeb.EmpleadoController do
  use PlanillafinalWeb, :controller

  alias Planillafinal.Planilla
  alias Planillafinal.Planilla.Empleado

  def index(conn,  %{"empresa" => empresa}) do
    empleados = Planilla.list_empleados_by_empresa(empresa)
    empresa = Planilla.get_empresa!(empresa)
    render(conn, "index.html", empleados: empleados, empresa: empresa)
  end

  def new(conn, %{"empresa" => empresa}) do
    changeset = Planilla.change_empleado(%Empleado{ empresa: empresa})
    pempresa = Planilla.get_empresa!(empresa)
    render(conn, "new.html", changeset: changeset, empresa: pempresa)
  end

  def create(conn, %{"empleado" => empleado_params, "id" => empresa }) do
    pempresa = Planilla.get_empresa!(empresa)
    case Planilla.create_empleado(empleado_params) do
      {:ok, empleado} ->
        conn
        |> put_flash(:info, "Empleado created successfully.")
        |> redirect(to: Routes.empleado_path(conn, :show, empleado))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, empresa: pempresa)
    end
  end

  def show(conn, %{"id" => id}) do
    empleado = Planilla.get_empleado!(id)
    render(conn, "show.html", empleado: empleado)
  end

  def edit(conn, %{"id" => id}) do
    empleado = Planilla.get_empleado!(id)
    empresa = Planilla.get_empresa!(empleado.empresa)
    changeset = Planilla.change_empleado(empleado)
    render(conn, "edit.html", empleado: empleado, changeset: changeset, empresa: empresa )
  end

  def update(conn, %{"id" => id, "empleado" => empleado_params}) do
    empleado = Planilla.get_empleado!(id)

    case Planilla.update_empleado(empleado, empleado_params) do
      {:ok, empleado} ->
        conn
        |> put_flash(:info, "Empleado updated successfully.")
        |> redirect(to: Routes.empleado_path(conn, :show, empleado))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", empleado: empleado, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    empleado = Planilla.get_empleado!(id)
    empresa = Planilla.get_empresa!(empleado.empresa)
    {:ok, _empleado} = Planilla.delete_empleado(empleado)

    conn
    |> put_flash(:info, "Empleado deleted successfully.")
    |> redirect(to: Routes.empleado_path(conn, :index, empresa.id))
  end
end
