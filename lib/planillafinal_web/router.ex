defmodule PlanillafinalWeb.Router do
  use PlanillafinalWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {PlanillafinalWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PlanillafinalWeb do
    pipe_through :browser

    get "/", PageController, :index


    get "/empresas", EmpresaController, :index
    get "/empresas/new", EmpresaController, :new
    post "/empresas", EmpresaController, :create
    get "/empresas/:id", EmpresaController, :show
    get "/empresas/:id/edit", EmpresaController, :edit
    put "/empresas/:id", EmpresaController, :update
    delete "/empresas/:id/delete", EmpresaController, :delete


    get "/empresas/:empresa/empleados", EmpleadoController, :index
    get "/empresas/:empresa/empleados/new", EmpleadoController, :new
    post "/empresas/:id/empleados", EmpleadoController, :create
    get "/empresas/empleado/:id", EmpleadoController, :show
    get "/empresas/empleados/:id/edit", EmpleadoController, :edit
    put "/empresas/empleados/:id", EmpleadoController, :update
    delete "/empresas/empleados/:id/delete", EmpleadoController, :delete


    get "/empresas/:id/dashboard", EmpresaController, :dashboard
    get "/empresas/:id/dashboard/:opcion/:moneda", EmpresaController, :consulta



  end

  # Other scopes may use custom stacks.
  # scope "/api", PlanillafinalWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: PlanillafinalWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
