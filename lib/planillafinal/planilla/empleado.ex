defmodule Planillafinal.Planilla.Empleado do
  use Ecto.Schema
  import Ecto.Changeset

  schema "empleados" do
    field :edad, :integer
    field :nombre, :string
    field :profesion, :string
    field :sexo, :string
    field :sueldo, :decimal
    field :empresa, :integer

    timestamps()
  end

  @doc false
  def changeset(empleado, attrs) do
    empleado
    |> cast(attrs, [:nombre, :edad, :sexo, :profesion, :sueldo, :empresa])
    |> validate_required([:nombre, :edad, :sexo, :profesion, :sueldo, :empresa])
  end
end
