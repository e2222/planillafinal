defmodule Planillafinal.Planilla.Empresa do
  use Ecto.Schema
  import Ecto.Changeset

  schema "empresas" do
    field :nombre, :string

    timestamps()
  end

  @doc false
  def changeset(empresa, attrs) do
    empresa
    |> cast(attrs, [:nombre])
    |> validate_required([:nombre])
  end
end
