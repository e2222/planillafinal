defmodule Planillafinal.Planilla do
  @moduledoc """
  The Planilla context.
  """

  import Ecto.Query, warn: false
  alias Planillafinal.Repo

  alias Planillafinal.Planilla.Empleado

  @doc """
  Returns the list of empleados.

  ## Examples

      iex> list_empleados()
      [%Empleado{}, ...]

  """
  def list_empleados do
    Repo.all(Empleado)
  end

  @doc """
  Gets a single empleado.

  Raises `Ecto.NoResultsError` if the Empleado does not exist.

  ## Examples

      iex> get_empleado!(123)
      %Empleado{}

      iex> get_empleado!(456)
      ** (Ecto.NoResultsError)

  """
  def get_empleado!(id), do: Repo.get!(Empleado, id)

  @doc """
  Creates a empleado.

  ## Examples

      iex> create_empleado(%{field: value})
      {:ok, %Empleado{}}

      iex> create_empleado(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_empleado(attrs \\ %{}) do
    %Empleado{}
    |> Empleado.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a empleado.

  ## Examples

      iex> update_empleado(empleado, %{field: new_value})
      {:ok, %Empleado{}}

      iex> update_empleado(empleado, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_empleado(%Empleado{} = empleado, attrs) do
    empleado
    |> Empleado.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a empleado.

  ## Examples

      iex> delete_empleado(empleado)
      {:ok, %Empleado{}}

      iex> delete_empleado(empleado)
      {:error, %Ecto.Changeset{}}

  """
  def delete_empleado(%Empleado{} = empleado) do
    Repo.delete(empleado)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking empleado changes.

  ## Examples

      iex> change_empleado(empleado)
      %Ecto.Changeset{data: %Empleado{}}

  """
  def change_empleado(%Empleado{} = empleado, attrs \\ %{}) do
    Empleado.changeset(empleado, attrs)
  end

  alias Planillafinal.Planilla.Empresa

  @doc """
  Returns the list of empresas.

  ## Examples

      iex> list_empresas()
      [%Empresa{}, ...]

  """
  def list_empresas do
    Repo.all(Empresa)
  end

  @doc """
  Gets a single empresa.

  Raises `Ecto.NoResultsError` if the Empresa does not exist.

  ## Examples

      iex> get_empresa!(123)
      %Empresa{}

      iex> get_empresa!(456)
      ** (Ecto.NoResultsError)

  """
  def get_empresa!(id), do: Repo.get!(Empresa, id)

  @doc """
  Creates a empresa.

  ## Examples

      iex> create_empresa(%{field: value})
      {:ok, %Empresa{}}

      iex> create_empresa(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_empresa(attrs \\ %{}) do
    %Empresa{}
    |> Empresa.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a empresa.

  ## Examples

      iex> update_empresa(empresa, %{field: new_value})
      {:ok, %Empresa{}}

      iex> update_empresa(empresa, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_empresa(%Empresa{} = empresa, attrs) do
    empresa
    |> Empresa.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a empresa.

  ## Examples

      iex> delete_empresa(empresa)
      {:ok, %Empresa{}}

      iex> delete_empresa(empresa)
      {:error, %Ecto.Changeset{}}

  """
  def delete_empresa(%Empresa{} = empresa) do
    Repo.delete(empresa)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking empresa changes.

  ## Examples

      iex> change_empresa(empresa)
      %Ecto.Changeset{data: %Empresa{}}

  """
  def change_empresa(%Empresa{} = empresa, attrs \\ %{}) do
    Empresa.changeset(empresa, attrs)
  end


  def list_empleados_by_empresa(idEmpresa) do
    Empleado
      |> where(empresa: ^idEmpresa)
      |> Repo.all
  end
end
