defmodule Planillafinal.Repo do
  use Ecto.Repo,
    otp_app: :planillafinal,
    adapter: Ecto.Adapters.Postgres
end
